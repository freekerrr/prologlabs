﻿%:- dynamic player/8.

player('Иванов Иван', 'Спартак', 13, 'з', 205, 90, 3, 'ГГПИ').
player('Петров Петр', 'Динамо', 96, 'пз', 185, 78, 4, 'ГТК').
player('Сидоров Денис', 'Локомотив', 69, 'в', 190, 88, 2, 'ГТУ').
player('Васечкин Илья', 'Торпедо', 5, 'в', 195, 80, 5, 'ГГПИ').
player('Алексеев Дима', 'ЦСКА', 1, 'н', 165, 67, 2, 'ГТК').
player('Карпов Павел', 'Зенит', 12, 'н', 170, 74, 1, 'ГКК').

writeLine(S) :- write(S), nl.

menu :-
	writeLine(' ************************************* '),
	writeLine(' * 1. Добавление нового игрока в БД '),
	writeLine(' * 2. Удаление игрока из БД '),
	writeLine(' * 3. Просмотр данных '),
	writeLine(' * 4. Выход из программы '),
	writeLine(' * '),
	writeLine(' * 5. Список игроков '),
	writeLine(' * 6. Save to file '),
	writeLine(' * 7. Запрос по критерию '),
	writeLine(' ************************************* '),
	write(' Выбор: '),
	read_string(user_input, "\n", "\r", _, NAME),
	term_string(O, NAME),
	process(O).

displayPlayer(NAME) :-
	player(NAME, C, N, R, HEIGHT, W, NFL, COLLEGE),
	write(NAME),
	write(': ['),
		write(C), write(', '),
		write(N), write(', '),
		write(R), write(', '),
		write(HEIGHT), write(', '),
		write(W), write(', '),
		write(NFL), write(', '),
		write(COLLEGE),
	write(']'), nl.

iteratePlayersForDisplay([]).
iteratePlayersForDisplay([H|T]) :-
	displayPlayer(H),
	iteratePlayersForDisplay(T).

%% player('Карпов Павел', 'Зенит', 12, 'н', 170, 74, 1, 'ГКК').
iteratePlayersForSave([], _).
iteratePlayersForSave([H|T], STREAM) :-
	player(H, C, N, R, HEIGHT, W, NFL, COLLEGE),
	swritef(S, "player('%w', '%w', %w, '%w', %w, %w, %w, '%w').\n", [H, C, N, R, HEIGHT, W, NFL, COLLEGE]),
	write(STREAM, S),
	iteratePlayersForSave(T, STREAM).

process(1) :-
	writeLine('Добавление игрока'),
	write('Имя: '), read(NAME),
	write('Клуб: '), read(CLUB),
	write('Номер: '), read(NUMBER),
	write('Роль: '), read(ROLE),
	write('Рост: '), read(HEIGHT),
	write('Вес: '), read(WEIGHT),
	write('Стаж: '), read(EXP),
	write('Где: '), read(COLLEGE),
	assertz(player(NAME, CLUB, NUMBER, ROLE, HEIGHT, WEIGHT, EXP, COLLEGE)),
	write('Был добавен - '), displayPlayer(NAME), menu.

process(2) :-
	write("Введите имя удаляемого игрока: "),
	read_string(user_input, "\n", "\r", _, NAME),
	term_string(N, NAME),
	retract(player(N, _, _, _, _, _, _, _)),
	write(N), write(" удален."), nl, menu.

process(3) :-
	write("Введите имя для просмотра: "),
	read_string(user_input, "\n", "\r", _, NAME),
	term_string(N, NAME),
	displayPlayer(N), menu.

process(4) :- !.

process(5) :-
	findall(N, player(N, _, _, _, _, _, _, _), Ns),
	iteratePlayersForDisplay(Ns), menu.

process(6) :-
	open('players.pl', write, S),
	findall(N, player(N, _, _, _, _, _, _, _), Ns),
	iteratePlayersForSave(Ns, S),
	close(S), menu.


process(7) :-
	writeLine("1) Показать всех вратарей"),
	writeLine("2) Показать всех игроков выше 180см"),
	writeLine("3) Показать всех с двухлетним опытом"),
	write(' Выбор: '), read(O), criteria(O), menu.


p(Player, R) :- player(Player, _, _, R, _, _, _, _).
allGatekeepers(Gatekeepers) :-
	findall(Player,
              (
		 p(Player, R),
                 R = в
              ),
              Gatekeepers).

t(Player, R) :- player(Player, _, _, _, R, _, _, _).
allTall(T) :-
	findall(Player,
              (
		 t(Player, R),
                 R > 180
              ),
              T).

g(Player, R) :- player(Player, _, _, _, _, _, R, _).
allTwoYearsExp(T) :-
	findall(Player,
              (
		 g(Player, R),
                 R == 2
              ),
              T).

criteria(1) :- allGatekeepers(G), writeLine(G).
criteria(2) :- allTall(T), writeLine(T).
criteria(3) :- allTwoYearsExp(R), allTwoYearsExp(R).

%% process(_) :- menu().

go() :-
	menu.
