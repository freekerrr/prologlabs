chet([],0):- !. 
chet([N|T], A):-chet(T,A1), ((N mod 2 =:= 1) -> A is A1+1; A is A1+0).
