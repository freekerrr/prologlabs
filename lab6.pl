% 0 5
% 3 2
% 0 2
% 2 0
% 2 5
% 3 4
consist(A, B) :-
write('bottel1: '), write(A),
write(' bottel2: '), write(B), nl.

pto3(A, B, RA, RB) :-
Sum is (A + B),
(Sum > 3 ->
(RA is 3, RB is (Sum - 3));
(RA is Sum, RB is 0)),
consist(RA, RB).

sliv3(_, B, RA, RB) :-
RA is 0,
RB is B,
consist(RA, RB).

vliv5(A, _, RA, RB) :-
RA is A,
RB is 5,
consist(RA, RB).

lab :-
vliv5(0, 5, A1, B1),
pto3(A1, B1, A2, B2),
sliv3(A2, B2, A3, B3),
pto3(A3, B3, A4, B4),
vliv5(A4, B4, A5, B5),
pto3(A5, B5, _, _).
