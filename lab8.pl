﻿rule(1, 'Google', [1, 2, 3, 5, 6, 7]).
rule(2, 'Apple', [2, 3, 5, 6, 7]).
rule(3, 'Asus', [3, 4, 5, 6, 7]).
rule(4, 'HTC', [2, 5, 6]).
rule(5, 'Xiaimo', [1, 2, 4, 5]).
rule(6, 'Fly', [1, 3, 4, 5, 6, 7]).
rule(7, 'Nokia', [1, 2, 4, 5]).


cond(1, 'Металлический корпус').
cond(2, 'Красивый дизайн').
cond(3, 'Качественная камера').
cond(4, 'Относительно низкая стоимость').
cond(5, 'Высокая производительность').
cond(6, 'Больше 4ГБ оперативной памяти').
cond(7, 'Долговечность').
%% Utils

countAll(Y) :-
	findall(N, rule(_, N, _), Ns),
	length(Ns, X),
	Y is X - 1.


addToList(ITEM, LIST, [ITEM|LIST]).
copyList(LIST, LIST).
max_list(L, M, I) :- nth0(I, L, M), \+ (member(E, L), E > M).
reverse([], Z, Z).
reverse([H | T], Z, A) :- reverse(T, Z, [H | A]).

inc(X, Y) :- Y is X+1.
writeLine(S) :- write(S), nl.

%% Logic

printAll() :-
	findall(N, rule(_, N, _), Ns),
	write(Ns).

pick(8, A, A).
pick(I, A, LIST) :-
	cond(I, MESSAGE),
	write(MESSAGE), write('?'), nl,
	read(ANSWER),
	(ANSWER == d ->
		addToList(I, A, R)
	;
		copyList(A, R)
	),
	inc(I, NEXT_I),
	pick(NEXT_I, R, LIST).

pick(LIST) :-
	pick(1, [], LIST).

getMatches(N, CRITERIAS, MATCHES) :-
	rule(N, _, PROPERTIES),
	list_to_set(PROPERTIES, PROPERTIES_SET),
	list_to_set(CRITERIAS, CRITERIAS_SET),
	intersection(PROPERTIES_SET, CRITERIAS_SET, INTERSECTION),
	length(INTERSECTION, MATCHES).

createMatchesList(7, _, A, A).
createMatchesList(I, CRITERIAS, A, LIST) :-
	getMatches(I, CRITERIAS, MATCHES),
	addToList(MATCHES, A, R),
	inc(I, NEXT_I),
	createMatchesList(NEXT_I, CRITERIAS, R, LIST).

getResult(CRITERIAS, NAME) :-
	createMatchesList(1, CRITERIAS, [], L),
	reverse(L, LIST, []),
	max_list(LIST, _, II),
	inc(II, I),
	rule(I, NAME, _).

go() :-
	writeLine('1) - Показать всех производителей smartphons'),
	writeLine('2) - Выбрать подходящий'),
	write('Выбор: '), read(O), process(O).

process(1) :- printAll(), nl, go().
process(2) :-
	nl, write('Выберите самые важные для вас критерии, отвечая d (Да) или n (Нет):'), nl,
	pick(LIST),
	getResult(LIST, NAME),
	write('Самый подходящий вам smartphone производит '), write(NAME), nl, !.

