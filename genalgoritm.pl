﻿go() :- 
    randGen(G, 10),
    write("1. Формируем начальную популяцию: "), nl,
    write("----------------------------------------"), nl,
    printGen(G), nl,
    write("2. Определяем приспособленность каждой особи: "), nl,
    write("----------------------------------------"), nl,
    fitness(G, 10, F), write("fitness: "), write(F), nl,
    write("Максимальный ранг: "), maxRankF(F, 10, M), write(M), nl,
    write("Особи двух высших рангов : "),  bestindex(G, M, Best1), bestOf(Best1, Best), write(Best) , write(" - переходящие в следующее поколение: "), nl,
    nl, write("3. Кроссовер: "), nl,
    write("----------------------------------------"),
    crossing(G, Best, G1), nl, nl,
    nl, write("4. Мутация: "), nl,
    write("----------------------------------------"),
    mutation(G1, NewG), nl,
    nl, write("5. Новая популяция: "), nl,
    write("----------------------------------------"), nl,
    printGen(NewG),
    write("----------------------------------------"), !.

rand(R):-
    random(H), R is round(H).
randO([H|T], N):-
    rand(H),
    N1 is N - 1,
    (N1 > 0 -> randO(T, N1); T = []).
randGen([H|T], N):-
    randO(H, 8), 
    N1 is N - 1,
    (N1 > 0 -> randGen(T, N1); T = []).

printG([], _) :- !.
printG([H|T], N) :-
    write(" Individ "), write(N), write(": "), writeln(H),
    N1 is N + 1,
    printG(T, N1).
printGen(G) :-
    printG(G, 0).

sumlist([H|[]], S):- S = H, !.
sumlist([H|T], S):-
    sumlist(T, S1),
    S is S1 + H.
%sumGen([H|_]):-
%    sumlist(H, S), write(S).

fitness([H|[]], M, F):-
    sumlist(H, S),
    R is S / M, F = [R], !.
fitness([H|T], M, F):-
    fitness(T, M, L),
    fitness([H], M, R),
    append(R,L,F).

maxRank([H|[]], M):- M = H, !.
maxRank([H|T], M):-
    maxRank(T, M1),
    M1 > H -> M = M1; M = H.
maxRankF(F, N, M):-
    maxRank(F, M1), M is round(M1 * N).

add_tail([],X,[X]).
add_tail([H|T],X,[H|L]):-add_tail(T,X,L).
    
add_head([],F,[F]). 
add_head([X|XS],F,[F,X|XS]):- add_head(XS,F,YS). 

bestindexOf([H|[]], MaxR, 0, Best):-
    sumlist(H, S), (S = MaxR -> add_tail(_, 0, Best); Best = []), !.
bestindexOf([H|T], MaxR, Index, Best):-
    bestindexOf(T, MaxR, Index1, Best1),
    Index is Index1+1,
    sumlist(H, S),
    (S = MaxR -> add_tail(Best1, Index, Best); Best = Best1).
bestindex(G, M, Best):-
    bestindexOf(G, M, Index, Best1), M1 is M - 1, bestindexOf(G, M1, Index, Best2), append(Best1, Best2, Best).

uniformCross([One|[]], [Two|[]], NewO):-
    rand(R), (R = 1 -> add_tail(_, One, NewO); add_tail(_, Two, NewO)).
uniformCross([One|T1], [Two|T2], NewO):-
    uniformCross(T1, T2, NewO1), 
    rand(R), (R = 1 -> add_tail(NewO1, One, NewO); add_tail(NewO1, Two, NewO)).

cross(G, [First, Second|[]], NewG):-
    nth0(First, G, O1), nth0(Second, G, O2), 
    uniformCross(O1, O2, NewO1), uniformCross(O2, O1, NewO2),
    uniformCross(O1, O2, NewO3), uniformCross(O2, O1, NewO4),
    watchCross(O1, O2, NewO1), watchCross(O2, O1, NewO2),
    watchCross(O1, O2, NewO3), watchCross(O2, O1, NewO4),
    add_tail(_, NewO1, NewG1), add_tail(NewG1, NewO2, NewG2),
    add_tail(NewG2, NewO3, NewG3), add_tail(NewG3, NewO4, NewG), !.
cross(G, [First, Second|T], NewG):-
    cross(G, T, NewG1),
    nth0(First, G, O1), nth0(Second, G, O2),
    uniformCross(O1, O2, NewO1), watchCross(O1, O2, NewO1), 
    uniformCross(O2, O1, NewO2), watchCross(O2, O1, NewO2),
    add_tail(_, NewO1, NewG2), add_tail(NewG2, NewO2, NewG4),
    append(NewG1, NewG4, NewG), !.

crossing(G, Best, NewG):-
    length(Best, L),
    (L =< 4 -> 
        (addBest(G, Best, NewG1), cross(G, Best, NewG2), append(NewG1, NewG2, NewG));
        (take(Best, 0, 4, NewBest), addBest(G, NewBest, NewG1),cross(G, NewBest, NewG2),append(NewG1, NewG2, NewG))).

addBest(G, [H|[]], NewG):-
    nth0(H, G, O),
    add_tail(_, O, NewG), !.
addBest(G, [H|T], NewG):-
    addBest(G, T, NewG1),
    nth0(H, G, O),
    add_tail(NewG1, O, NewG).

take(L, B, E, NL):-
    B = E -> !; 
    B1 is B + 1,
    (B1 < E -> take(L, B1, E, NL1); !),  
    nth0(B, L, El),
    add_head(NL1, El, NL).
    
bestOf(Best, NewBest):-
    length(Best,L),
    (L = 4 -> NewBest = Best;
        (L > 4 -> take(Best, 0, 4, NewBest);
            addto(Best, Best1), bestOf(Best1, NewBest))).

addt([], OldB, R, NB):-
    add_tail(OldB, R, NB), !.
addt([H|[]], OldB, R, NB):-
    (R = H -> addto(OldB, NB); add_tail(OldB, R, NB), !).
addt([H|T], OldB, R, NB):-
    (R = H -> addto(OldB, NB); addt(T, OldB, R, NB)).
addto(B, NB):-
    random_between(0, 8, R), addt(B, B, R, NB).

watchCross(O1, O2, NewO):-
    nl, write("Скрещиваем две особи: "), write(O1), write(" "), write(O2), write(" --> "), write(NewO).

watchMutation(F11, F12, F21, F22, NG):-
    nl, write("Случайная мутация пары: "), write(F11), write(F12), write(" и "), write(F21), write(F22), write(" --> "), write(NG).

mutat(G, [First, Second|[]], NewG):-
    nth0(First, G, O1),nth0(Second, G, O2), mut(O1, O2, NG), append(_, NG, NewG).
mutat(G, [First, Second| T], NewG):-
    mutat(G, T, NewG1),
    nth0(First, G, O1),nth0(Second, G, O2),
    mut(O1,O2,NG), append(NewG1, NG, NewG).
mut(O1, O2, NG):-
    random_between(1, 6, R), 
    take(O1, 0, R, F11), take(O1, R, 8, F12),
    take(O2, 0, R, F21), take(O2, R, 8, F22),
    append(F22, F11, NO1), append(F12, F21, NO2),
    add_tail(_, NO1, NewG1), add_tail(NewG1, NO2, NG), watchMutation(F11, F12, F21, F22, NG). 
    
addMut(G, [N|[]], [E|[]], NewG):-
    insertIn(E, N, G, NewG), !.   
addMut(G, [N|NT], [E|ET], NewG):-
    addMut(G, NT, ET, NewG1),
    insertIn(E, N, NewG1, NewG), !.

insertIn(E, N, X, Y):-
    N1 is N + 1,
    take(X, 0, N, X1), take(X, N1, 10, X2),
    add_tail(X1, E, X3), append(X3, X2, Y).

mutation(G, NewG):-
    bestOf([], RL), mutat(G, RL, NG), nl, write("RL: "), write(RL), addMut(G, RL, NG, NewG).