color(blue).
color(white).
color(green).

girl(anna).
girl(vala).
girl(natasha).

look(X, Y, Z) :- girl(X), color(Y), color(Z),
                 X = vala, not(Y = white), not(Z = white),
                 not(look(natasha, Y, Z)), not(look(anna, Y, Z)).

look(X, Y, Z) :- girl(X), color(Y), color(Z),
                 X = natasha, Z = green,
                 not(look(anna, Y, Z)), not(look(vala, Y, Z)).

look(X, Y, Z) :- girl(X), color(Y), color(Z),
                 X = anna, Y = Z,
                 not(look(vala, Y, Z)), not(look(natasha, Y, Z)).

looks :- look(anna, X,Y), printGirl(anna,X,Y),
         look(vala, O,G), printGirl(vala,O,G),
         look(natasha, Z,D), printGirl(natasha,Z,D).

printGirl(X, Y, Z):- write(X),
                     write(' dress is: '), write(Y),
                     write(' shose is:  '), write(Z), nl.
